import nltk
import multiprocessing

from .querymatcherworker import QueryMatcherWorker
from ..utils.fileutils import count_length

class QueryMatcher:
    CORPUS_SIZE = 950489 # 100 * 10000
    AVG_DOC_LENGTH = 650

    def __init__(self, index_file):
        self.index_file = index_file

    def initialize(self):
        nltk.download("rslp", quiet=True)
        nltk.download("stopwords", quiet=True)
        nltk.download("punkt", quiet=True)

    def merge_query_doc_sims(self, sims_a, sims_b):
        sims_a = sorted(sims_a)
        sims_b = sorted(sims_b)
        a_i = 0
        b_i = 0

        similarities = {}
        while a_i < len(sims_a) or b_i < len(sims_b):
            if a_i >= len(sims_a):
                doc_b, doc_sim_b = sims_b[b_i]
                similarities[doc_b] = similarities.get(doc_b, 0) + doc_sim_b
                b_i += 1
            elif b_i >= len(sims_b):
                doc_a, doc_sim_a = sims_a[a_i]
                similarities[doc_a] = similarities.get(doc_a, 0) + doc_sim_a
                a_i += 1
            else:
                doc_a, doc_sim_a = sims_a[a_i]
                doc_b, doc_sim_b = sims_b[b_i]
                
                if doc_a == doc_b:
                    similarities[doc_a] = similarities.get(doc_a, 0) + doc_sim_a + doc_sim_b
                    a_i += 1
                    b_i += 1
                elif doc_a < doc_b:
                    similarities[doc_a] = similarities.get(doc_a, 0) + doc_sim_a
                    a_i += 1
                else:
                    similarities[doc_b] = similarities.get(doc_b, 0) + doc_sim_b
                    b_i += 1

        return similarities.items()

    def match(self, queries, algorithm, n_processes=1):
        manager = multiprocessing.Manager()

        file_length = count_length(self.index_file)
        process_length = file_length // n_processes

        cummulative_query_doc_sims = manager.list()

        processes = []
        for pid in range(n_processes):
            processes.append(multiprocessing.Process(target=QueryMatcherWorker.run, args=(
                queries, # queries,
                self.index_file, # index_file,
                algorithm, # algorithm
                pid * process_length, # starting_point,
                process_length, # read_length,
                QueryMatcher.CORPUS_SIZE, # corpus_size,
                QueryMatcher.AVG_DOC_LENGTH, # avgdl
                cummulative_query_doc_sims, # cummulative_query_doc_sims
            )))

        for process in processes:
            process.start()

        for process in processes:
            process.join()

        query_doc_rankings = {}
        for query_id, query in enumerate(queries):
            query_doc_sims_parts = [qds[query_id] for qds in cummulative_query_doc_sims]
            query_doc_sims = []

            for i in range(len(query_doc_sims_parts)):
                query_doc_sims = self.merge_query_doc_sims(query_doc_sims, query_doc_sims_parts[i])

            query_ranking = sorted(query_doc_sims, key=lambda qr: qr[1], reverse=True)
            query_doc_rankings[query] = query_ranking[:10]

        return query_doc_rankings