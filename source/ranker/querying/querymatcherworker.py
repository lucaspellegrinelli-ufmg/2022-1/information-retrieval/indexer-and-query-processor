import math

from nltk.stem import RSLPStemmer

from ..utils.indexutils import parse_index_line, find_line
from ..utils.languageutils import get_tokens

class QueryMatcherWorker:
    @staticmethod
    def run(queries, index_file, algorithm, starting_point, read_length, corpus_size, avgdl, cummulative_query_doc_sims):
        stemmer = RSLPStemmer()
        
        query_doc_sim = [[] for _ in range(len(queries))]
        queries_tokens = [list(get_tokens(q, stemmer)) for q in queries]

        all_query_tokens = set()
        for query_tokens in queries_tokens:
            for token in query_tokens:
                all_query_tokens.add(token)

        with open(index_file, "rb") as f:
            for token in all_query_tokens:
                token_term, token_tf = token
                line = find_line(f, token_term, starting_point, starting_point + read_length)
                if line is not None:
                    term, values = parse_index_line(line)
                    term_presence = QueryMatcherWorker.check_term_presence(term, queries_tokens)

                    for query_id, query_tf in term_presence:
                        for doc_id, doc_tf, doc_len in values:
                            if algorithm == "TFIDF":
                                idf = math.log((corpus_size + 1) / len(values))
                                similarity = (doc_tf / doc_len) * query_tf * idf
                            else:
                                idf = math.log((corpus_size - len(values) + 0.5) / (len(values) + 0.5) + 1)
                                k = 1.5
                                b = 0.75
                                tf = (doc_tf / doc_len)
                                numerator = tf * (k + 1)
                                denomin = tf + k * (1 - b + b * doc_len / avgdl)
                                similarity = idf * numerator / denomin

                            query_doc_sim[query_id].append((doc_id, similarity))
                            
        cummulative_query_doc_sims.append(query_doc_sim)

    @staticmethod
    def check_term_presence(term, queries_tokens):
        idxs = []
        for query_i, query_tokens in enumerate(queries_tokens):
            for query_term, query_tf in query_tokens:
                if term == query_term:
                    idxs.append((query_i, query_tf))
                    break
        return idxs