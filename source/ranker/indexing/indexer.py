import os
import time
import json
import nltk
import shutil
import multiprocessing

from nltk.stem import RSLPStemmer

from .warcindexer import WARCIndexer
from .indexmerger import IndexMerger
from ..utils.indexutils import parse_index_line

class Indexer:
    PARTIAL_INDEX_MAX_SIZE = 1e6
    TEMP_FILES_FOLDER = "index-tmps"

    def __init__(self, corpuspath, indexpath):
        self.corpuspath = corpuspath
        self.indexpath = indexpath

    def initialize(self):
        nltk.download("rslp", quiet=True)
        nltk.download("stopwords", quiet=True)
        nltk.download("punkt", quiet=True)
        nltk.download("words", quiet=True)
        nltk.download("mac_morpho", quiet=True)
        nltk.download("floresta", quiet=True)

    def find_warc_files(self):
        warc_paths = []
        for file in os.listdir(self.corpuspath):
            filename = os.fsdecode(file)
            if filename.endswith(".warc.gz.kaggle"):
                warc_paths.append(os.path.join(self.corpuspath, filename))
        return warc_paths

    def get_all_allowed_words(self):
        def is_permitted_word(word, stopwords):
            return len(word) > 1 and word not in stopwords

        all_words = set()
        all_words.update(w.lower() for w in nltk.corpus.floresta.words() if w)
        all_words.update(w.lower() for w in nltk.corpus.mac_morpho.words() if w)
        all_words.update(w.lower() for w in nltk.corpus.words.words() if w)

        stemmer = RSLPStemmer()
        stopwords = set(nltk.corpus.stopwords.words("portuguese"))
        return set(stemmer.stem(w) for w in all_words if is_permitted_word(w, stopwords))
                
    def index_files(self, memory_limit, n_processes=1):
        start = time.time()

        manager = multiprocessing.Manager()
        warc_files = manager.list(enumerate(sorted(self.find_warc_files())))

        permitted_words = self.get_all_allowed_words()

        processes = []
        for pid in range(n_processes):
            processes.append(multiprocessing.Process(target=WARCIndexer.run, args=(
                pid, # indexer_id
                Indexer.TEMP_FILES_FOLDER, # tmp_folder
                warc_files, # warc_paths
                permitted_words, # allowed words
                memory_limit // n_processes, # mem limit
                10000, # docs_per_warc
                (Indexer.PARTIAL_INDEX_MAX_SIZE // n_processes) * memory_limit / 1024 # partial_index_max_size
            )))

        for process in processes:
            process.start()

        for process in processes:
            process.join()

        merger = IndexMerger(
            indexes_folder=Indexer.TEMP_FILES_FOLDER,
            merge_folder="merged",
            index_file=self.indexpath,
            urls_file="corpus-url.txt",
            memory_limit=memory_limit
        )
        merger.merge_indexes(n_processes=8)
        merger.merge_urls(n_processes=8)
        shutil.rmtree(Indexer.TEMP_FILES_FOLDER)

        n_lists = 0
        list_sizes = 0
        with open(self.indexpath) as index:
            line = index.readline()
            while len(line) > 0:
                key, values = parse_index_line(line)
                n_lists += 1
                list_sizes += len(values)
                line = index.readline()


        print(json.dumps({
            "Index Size": round(os.path.getsize(self.indexpath) / (1024 * 1024), 1),
            "Elapsed Time": int(time.time() - start),
            "Number of Lists": n_lists,
            "Average List Size": round(list_sizes / n_lists, 1)
        }))