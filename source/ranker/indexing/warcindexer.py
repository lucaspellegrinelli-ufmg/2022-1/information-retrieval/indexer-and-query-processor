import os

from nltk.stem import RSLPStemmer
from warcio.archiveiterator import ArchiveIterator

from ..utils.resourceutils import set_memory_limit
from ..utils.indexutils import create_line_from_item
from ..utils.languageutils import get_tokens

class WARCIndexerData:
    def __init__(self):
        self.docs_url = {}
        self.partial_index = {}
        self.partial_index_length = 0
        self.temp_index_count = 0

    def clear(self):
        self.docs_url.clear()
        self.partial_index.clear()
        self.partial_index_length = 0

    def increase_temp_index(self):
        self.temp_index_count += 1
    
    def add_to_index(self, term, doc_id, tf, total_tokens, url):
        self.docs_url[doc_id] = url
        self.partial_index_length += 1
        if term in self.partial_index:
            self.partial_index[term].append((doc_id, tf, total_tokens))
        else:
            self.partial_index[term] = [(doc_id, tf, total_tokens)]

class WARCIndexer:
    @staticmethod
    def run(indexer_id, tmp_folder, warc_paths, allowed_words, memory_limit, docs_per_warc=10000, partial_index_max_size=1e6):
        set_memory_limit(memory_limit)
        
        data = WARCIndexerData()

        while len(warc_paths) > 0:
            warc_id, warc_path = warc_paths.pop(0)
            WARCIndexer.index_file(
                filepath=warc_path,
                indexer_id=indexer_id,
                starting_doc_id=warc_id * docs_per_warc,
                data=data,
                tmp_folder=tmp_folder,
                stemmer=RSLPStemmer(),
                allowed_words=allowed_words,
                partial_index_max_size=partial_index_max_size
            )

        WARCIndexer.save_index_entries(
            data=data,
            indexer_id=indexer_id,
            tmp_folder=tmp_folder
        )

    @staticmethod
    def save_index_entries(data, indexer_id, tmp_folder):
        urls_folder = os.path.join(tmp_folder, "urls")

        os.makedirs(tmp_folder, exist_ok=True)
        os.makedirs(urls_folder, exist_ok=True)

        with open(f"{tmp_folder}/index-tmp-{indexer_id}-{data.temp_index_count}.txt", "a+") as f:
            index_items = data.partial_index.items()
            index_items = sorted(index_items)
            for key, values in index_items:
                f.write(create_line_from_item(key, values))

        with open(f"{urls_folder}/tmp-{indexer_id}-{data.temp_index_count}.txt", "a+") as f:
            url_items = data.docs_url.items()
            url_items = sorted(url_items)
            for key, url in url_items:
                f.write(f"{key} {url}\n")

        data.increase_temp_index()
        data.clear()

    @staticmethod
    def index_file(filepath, indexer_id, starting_doc_id, data, tmp_folder, stemmer, allowed_words, partial_index_max_size):
        # print(f"Process {indexer_id} got 'WARC {filepath}'")
        
        with open(filepath, "rb") as stream:
            for doc_id, record in enumerate(ArchiveIterator(stream)):
                if record.rec_type == "response":
                    url = record.rec_headers.get_header("WARC-Target-URI")
                    content = record.content_stream().read().decode("utf-8")
                    content_tokens = list(get_tokens(content, stemmer, allowed_words))
                    total_tokens = sum(tf for _, tf in content_tokens)
                    for term, tf in content_tokens:
                        true_doc_id = starting_doc_id + doc_id
                        data.add_to_index(term, true_doc_id, tf, total_tokens, url)

                        if data.partial_index_length >= partial_index_max_size:
                            # print(f"Process {indexer_id} dumping partial index ({data.temp_index_count})")
                            WARCIndexer.save_index_entries(
                                data=data,
                                indexer_id=indexer_id,
                                tmp_folder=tmp_folder
                            )