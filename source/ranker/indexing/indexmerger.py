import os
import shutil
import multiprocessing

from ..utils.indexutils import parse_index_line, create_line_from_item, join_tuple_lists
from ..utils.fileutils import clear_file
from ..utils.resourceutils import set_memory_limit

def index_parse_func(line):
    return parse_index_line(line)

def index_merge_func(key, a_values, b_values):
    merged_values = join_tuple_lists(a_values, b_values)
    merged_line = create_line_from_item(key, merged_values)
    return merged_line

def urls_parse_func(line):
    key, url = line.split(" ", 1)
    return int(key), url

def urls_merge_func(key, a_url, b_url):
    return f"{key} {a_url}"

class IndexMerger:
    INDEX_PARSE_FUNC = index_parse_func
    INDEX_MERGE_FUNC = index_merge_func
    URLS_PARSE_FUNC = urls_parse_func
    URLS_MERGE_FUNC = urls_merge_func

    def __init__(self, indexes_folder, merge_folder, index_file, urls_file, memory_limit):
        self.indexes_folder = indexes_folder
        self.merge_folder = merge_folder
        self.index_file = index_file
        self.urls_file = urls_file
        self.memory_limit = memory_limit

    def merge_indexes(self, n_processes=1):
        self.full_merge(
            dirname=self.indexes_folder,
            outfile=self.index_file,
            parse_func="index",
            merge_func="index",
            n_processes=n_processes
        )

    def merge_urls(self, n_processes=1):
        self.full_merge(
            dirname=os.path.join(self.indexes_folder, "urls"),
            outfile=self.urls_file,
            parse_func="urls",
            merge_func="urls",
            n_processes=n_processes
        )

    def find_all_txt_files(self, dirpath):
        paths = []
        for file in os.listdir(dirpath):
            filename = os.fsdecode(file)
            if filename.endswith(".txt"):
                paths.append(os.path.join(dirpath, filename))
        return paths

    def full_merge(self, dirname, outfile, parse_func, merge_func, n_processes):
        iteration = 0
        created_files = self.merge_iteration(
            from_folder=dirname,
            to_folder=os.path.join(dirname, self.merge_folder, f"merged-{iteration}"),
            depth=iteration,
            parse_func=parse_func,
            merge_func=merge_func,
            n_processes=n_processes
        )

        while len(created_files) > 1:
            iteration += 1
            created_files = self.merge_iteration(
                from_folder=os.path.join(dirname, self.merge_folder, f"merged-{iteration - 1}"),
                to_folder=os.path.join(dirname, self.merge_folder, f"merged-{iteration}"),
                depth=iteration,
                parse_func=parse_func,
                merge_func=merge_func,
                n_processes=n_processes
            )

        final_merged_file = self.find_all_txt_files(os.path.join(dirname, self.merge_folder, f"merged-{iteration}"))[0]
        shutil.copy(final_merged_file, outfile)
        shutil.rmtree(os.path.join(dirname, self.merge_folder))

    def merge_iteration(self, from_folder, to_folder, depth, parse_func, merge_func, n_processes):
        os.makedirs(to_folder, exist_ok=True)

        all_indexes_paths = self.find_all_txt_files(from_folder)

        if len(all_indexes_paths) % 2 == 1:
            file_id = len(all_indexes_paths) // 2
            file_name = os.path.join(to_folder, f"merged-{file_id}.txt")
            shutil.copy(all_indexes_paths.pop(), file_name)

        manager = multiprocessing.Manager()
        processes_tasks = []
        for i in range(0, len(all_indexes_paths), 2):
            index_a = all_indexes_paths[i]
            index_b = all_indexes_paths[i + 1]
            outfile = os.path.join(to_folder, f"merged-{i // 2}.txt")
            processes_tasks.append((outfile, index_a, index_b, to_folder, parse_func, merge_func))
        
        shared_processes_tasks = manager.list(processes_tasks)

        processes = [
            multiprocessing.Process(
                target=IndexMerger.merge_task,
                args=(
                    shared_processes_tasks,
                    self.memory_limit // n_processes
                )
            ) for pid in range(min(n_processes, len(processes_tasks)))
        ]

        for process in processes:
            process.start()

        for process in processes:
            process.join()

        if depth > 0:
            shutil.rmtree(from_folder)

        return self.find_all_txt_files(to_folder)

    @staticmethod
    def merge_task(tasks, memory_limit):
        set_memory_limit(memory_limit)

        while len(tasks) > 0:
            IndexMerger.merge_files(*tasks.pop(0))

    @staticmethod
    def merge_files(outfile, index_a, index_b, to_folder, parse_func, merge_func):
        if not callable(parse_func):
            if parse_func == "index":
                parse_func = IndexMerger.INDEX_PARSE_FUNC
            elif parse_func == "urls":
                parse_func = IndexMerger.URLS_PARSE_FUNC
        
        if not callable(merge_func):
            if merge_func == "index":
                merge_func = IndexMerger.INDEX_MERGE_FUNC
            elif merge_func == "urls":
                merge_func = IndexMerger.URLS_MERGE_FUNC

        fa = open(index_a, "r")
        fb = open(index_b, "r")
        clear_file(outfile)
        merged = open(outfile, "a+")

        # Merging
        line_a = fa.readline()
        line_b = fb.readline()
        while len(line_a) > 0 or len(line_b) > 0:
            if len(line_a) == 0:
                merged.write(line_b)
                line_b = fb.readline()
            elif len(line_b) == 0:
                merged.write(line_a)
                line_a = fa.readline()
            else:
                line_a_key, line_a_values = parse_func(line_a)
                line_b_key, line_b_values = parse_func(line_b)

                if line_a_key == line_b_key:
                    merged.write(merge_func(line_a_key, line_a_values, line_b_values))
                    line_a = fa.readline()
                    line_b = fb.readline()
                elif line_a_key < line_b_key:
                    merged.write(line_a)
                    line_a = fa.readline()
                else:
                    merged.write(line_b)
                    line_b = fb.readline()

        fa.close()
        fb.close()
        merged.close()