from collections import Counter
from nltk.tokenize import word_tokenize

def get_tokens(s, stemmer, allowed_words=None):
    tokens = word_tokenize(s.lower(), language="portuguese")
    tokens_tf = Counter(tokens).items()
    for token, tf in tokens_tf:
        stemmed_token = stemmer.stem(token)
        if allowed_words is not None and stemmed_token not in allowed_words: continue
        yield stemmed_token, tf