import math

def count_number_of_lines(path):
    return sum(1 for line in open(path))

def count_length(path):
    return sum(len(line) for line in open(path))

def find_in_lines(path, targets):
    return { i: line.strip() for i, line in enumerate(open(path)) if i in targets }

def clear_file(path):
    file = open(path, "w+")
    file.write("")
    file.close()
