import resource

def set_memory_limit(value):
    limit = value * 1024 * 1024
    resource.setrlimit(resource.RLIMIT_AS, (limit, limit))