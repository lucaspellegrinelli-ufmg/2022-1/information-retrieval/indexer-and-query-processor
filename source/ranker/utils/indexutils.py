def parse_index_line(line):
    key, entries = line.split(" ", 1)
    return key, parse_tuple_list_str(entries)

def create_line_from_item(key, vals):
    return key + " " + tuple_list_to_str(vals) + "\n"

def parse_tuple_str(s):
    def parse_number(x):
        return float(x) if "." in x or "e" in x else int(x) if isinstance(x, str) else x

    return [parse_number(x) for x in s.strip()[1:-1].split(",")]

def parse_tuple_list_str(s):
    return [parse_tuple_str(x) for x in s.strip().split(" ")]

def tuple_list_to_str(values):
    return " ".join(str(tuple(t)).replace(" ", "") for t in values)

def join_tuple_lists(tla, tlb):
    tla = sorted(tla)
    tlb = sorted(tlb)
    tla_i = 0
    tlb_i = 0
    out = []

    while tla_i < len(tla) or tlb_i < len(tlb):
        if tla_i >= len(tla):
            out.append(tlb[tlb_i])
            tlb_i += 1
        elif tlb_i >= len(tlb):
            out.append(tla[tla_i])
            tla_i += 1
        else:
            doc_a, tf_a, len_a = tla[tla_i]
            doc_b, tf_b, len_b = tlb[tlb_i]
            
            if doc_a == doc_b:
                out.append((doc_a, tf_a + tf_b, len_a))
                tla_i += 1
                tlb_i += 1
            elif doc_a < doc_b:
                out.append((doc_a, tf_a, len_a))
                tla_i += 1
            else:
                out.append((doc_b, tf_b, len_b))
                tlb_i += 1
    
    return out

def find_line(file, target_key, min_c, max_c, get_line_key_func=None):
    if get_line_key_func == None:
        def get_line_key_func(line):
            key, values = parse_index_line(line)
            return key

    def get_line(file, seek_index):
        iterations = 0
        file.seek(seek_index, 0)
        while file.read(1) != b'\n':
            file.seek(-2, 1)
            iterations += 1
        return iterations, file.readline().decode()

    if min_c > 0:
        _, line = get_line(file, min_c)
        min_key = get_line_key_func(line)
        if target_key < min_key:
            return None
    else:
        file.seek(0)
        min_key = get_line_key_func(file.readline().decode())

    _, line = get_line(file, max_c)
    max_key = get_line_key_func(line)
    if target_key > max_key:
        return None

    iterc = 0
    while min_c <= max_c:
        iterc += 1
        mid = int(min_c + (max_c - min_c) / 2)
        iterations, line = get_line(file, mid)
        key = get_line_key_func(line)
        
        if key == target_key:
            return line
        
        if key < target_key:
            min_c = mid + 1
        else:
            max_c = mid - 1
  
    return None