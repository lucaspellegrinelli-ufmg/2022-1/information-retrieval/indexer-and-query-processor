import sys
import argparse

from ranker.indexing.indexer import Indexer
from ranker.utils.resourceutils import set_memory_limit

def main():
    indexer = Indexer(args.corpus, args.index)
    indexer.initialize()
    indexer.index_files(memory_limit=args.memory_limit, n_processes=12)
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process some integers.")
    parser.add_argument(
        "-m",
        dest="memory_limit",
        action="store",
        required=True,
        type=int,
        help="memory available"
    )
    parser.add_argument(
        "-c",
        dest="corpus",
        action="store",
        required=True,
        type=str,
        help="corpus folder path"
    )
    parser.add_argument(
        "-i",
        dest="index",
        action="store",
        required=True,
        type=str,
        help="path to the index file to be generated"
    )
    args = parser.parse_args()
    set_memory_limit(args.memory_limit)
    try:
        main()
    except MemoryError:
        sys.stderr.write("\n\nERROR: Memory Exception\n")
        sys.exit(1)

# You CAN (and MUST) FREELY EDIT this file (add libraries, arguments, functions and calls) to implement your indexer
# However, you should respect the memory limitation mechanism and guarantee
# it works correctly with your implementation