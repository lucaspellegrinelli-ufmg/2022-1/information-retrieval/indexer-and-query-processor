import os
import json
import argparse

from ranker.querying.querymatcher import QueryMatcher
from ranker.utils.indexutils import find_line

def create_results_field(ranking):
    def get_line_key_func(line):
        key, url = line.split(" ", 1)
        return int(key)

    doc_ids = [doc_id for doc_id, doc_score in ranking]
    doc_urls = {}
    url_path = os.path.join(os.path.dirname(args.index), "corpus-url.txt")
    with open(url_path, "rb") as f:
        file_size = sum(len(line) for line in f)
        f.seek(0)

        for did in doc_ids:
            target_line = find_line(f, did, 0, file_size, get_line_key_func)
            _, doc_url = target_line.split(" ", 1)
            doc_urls[did] = doc_url

        return [
            { "URL": doc_urls[doc_id], "Score": doc_score} for doc_id, doc_score in ranking
        ]

def main():
    with open(args.queries, "r") as f:
        queries = f.readlines()
        matcher = QueryMatcher(args.index)
        rankings = matcher.match(queries, args.ranker, n_processes=12)

        for query, ranking in rankings.items():
            print(json.dumps({
                "Query": query,
                "Results": create_results_field(ranking)
            }))
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process some integers.")
    parser.add_argument(
        "-i",
        dest="index",
        action="store",
        required=True,
        type=str,
        help="the path to an index file"
    )
    parser.add_argument(
        "-q",
        dest="queries",
        action="store",
        required=True,
        type=str,
        help="the path to a file with the list of queries to process"
    )
    parser.add_argument(
        "-r",
        dest="ranker",
        action="store",
        required=True,
        choices=["TFIDF", "BM25"],
        help="a string informing the ranking function (either “TFIDF” or “BM25”) to be used to score documents for each query"
    )
    args = parser.parse_args()
    main()
